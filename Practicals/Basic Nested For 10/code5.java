/*
 Q5 Write a program to print the following pattern

    Number of rows = 3
    1A 1A 1A
    1A 1A 1A
    1A 1A 1A
    
    Number of rows = 4
    1A 1A 1A 1A
    1A 1A 1A 1A
    1A 1A 1A 1A
    1A 1A 1A 1A

 */

import java.util.*;
class code5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = 1;
        char ch = 'A';
        System.out.print("Enter the number of rows : ");
        int rows = sc.nextInt();
        for(int i=1;i<=rows;i++){
            for(int j=1;j<=rows;j++){
               System.out.print(num+""+ch+" ");
            }
            System.out.println();
        }
        sc.close();
    }  
}