/*
    Q9 Write a program to print the following pattern

    Number of rows = 4
    1 2 3 4
    4 5 6 7
    7 8 9 10
    10 11 12 13

    Number of rows = 3
    1 2 3
    3 4 5
    5 6 7
*/

import java.util.*;
class code9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows : ");
        int rows = sc.nextInt();
        int num = 2;
        for(int i=1;i<=rows;i++){
            num=num-1;
            for(int j=1;j<=rows;j++){
                System.out.print(num+" ");
                num++;
            }
            System.out.println();
        }
        sc.close();
    }  
}