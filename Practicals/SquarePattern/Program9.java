import java.util.*;
class SquareDemo{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter no of rows : ");
                int row=sc.nextInt();

                for(int i=1; i<=row; i++){
                        int num1=2;
			int num2=3;
                        for(int j=1; j<=row; j++){
                                if(i%2==1){
                                        if(j%2==1){
                                                System.out.print(num1*j+" ");
                                        }else{
                                                System.out.print(num2*j +" ");
                                        }
                                }else{
                                        if(j%2==1){
                                                System.out.print(num2*j+" ");
                                        }else{
                                                System.out.print(num1*j +" ");
                                        }
                                }
                        }
                        System.out.println();
                }
        }
}
