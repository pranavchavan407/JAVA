/*
8. Write a program where you have to store the employee's age working at a company,
take count of employees from the user.Take input from the user.

*/

import java.util.*;
class Code8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the Employee count : ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the age of Employee "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }

        System.out.println("Ages of employees:");
        for(int i=0;i<arr.length;i++){
            System.out.println("Employee "+(i + 1) + ": " + arr[i]);
        }
        sc.close();
    }   
}
