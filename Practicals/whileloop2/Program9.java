class WhileDemo{
        public static void main(String[]args){
                int num=2469185;
                int sum=0;
                int rem=0;

                while(num>0){
                        rem=num%10;
                        if(rem%2==1){
                                sum=sum+rem*rem;
                        }
                        num=num/10;
                }
                System.out.println(sum + " ");
        }
}
