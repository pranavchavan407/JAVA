import java.util.*;
class Code10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        for (int i = 0; i < arr.length; i++) {
            for(int j=i+1;j<arr.length;j++){
                if (arr[i]>arr[j]) {
                    int temp = arr[j];
                    arr[j]=arr[i];
                    arr[i] = temp;
                }
            }    
        }

        System.out.print("The second largest number is "+arr[size-2]);

        sc.close();
    }    
}
