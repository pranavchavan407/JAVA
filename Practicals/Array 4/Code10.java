import java.util.*;
class Code10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        char arr[] = new char[size];
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.next().charAt(0);
        }
        
        System.out.print("Enter character key: ");

        char ch = sc.next().charAt(0);

        int index = -1;

        for(int i=0;i<arr.length;i++){

            if (arr[i]==ch) {
                index = i;
            }    
        }
    
        if (index==-1) {
            System.out.println(ch+" does not occur in an array");
        }

        else if (index==0){
            System.out.println(ch+" is the first element in the array");
        }

        else{
            for(int i=0;i<index;i++){
                System.out.println(arr[i]);
            }
        }
        sc.close();
    }    
}
