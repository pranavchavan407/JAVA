import java.util.*;
class Code6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        char arr[] = new char[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.next().charAt(0);
        }
        
        int vowel = 0;
        int consonant = 0;

        for(int i=0;i<arr.length;i++){
            arr[i] = Character.toLowerCase(arr[i]);

            if (arr[i]=='a'||arr[i]=='e'||arr[i]=='i'||arr[i]=='o'||arr[i]=='u') {
                vowel++;
            }  
            
            else{
                consonant++;
            }
        }

        System.out.println("Count of vowels "+vowel);
        System.out.println("Count of consonant "+consonant);
    
        sc.close();
    }    
}
