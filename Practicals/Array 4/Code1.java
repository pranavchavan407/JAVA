import java.util.*;
class Code1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        int sum = 0;
        
        for(int i=0;i<arr.length;i++){
            sum+=arr[i];    
        }
        
        System.out.print("Array elements's average is "+sum/size);

        sc.close();
    }    
}
