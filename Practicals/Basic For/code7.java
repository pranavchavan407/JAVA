/**
 *  Write a program to print a table of 17 up to 170.
    Output: 17,34,51,68,...170
 */
class code7 {
    public static void main(String[] args){
        int x=17;
        for(int i=1;i<=10;i++){
            System.out.print(x*i + " ");
        }
    }   
}