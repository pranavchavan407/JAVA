/*
9. WAP to print the minimum element in the array, where you have to take the size and elements from the user.

Example:

Input:

Enter the array size: 5
Enter Elements 1: 5
Enter Elements 2: 6
Enter Elements 3: 9
Enter Elements 4: -9
Enter Elements 5: 17

Output:
Minimum number in the array is : -9

*/

import java.util.*;

class Code9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        
        int arr[] = new int[size];
        for(int i=0;i<arr.length;i++){
            System.out.print("Enter Elements "+(i+1)+": ");
            arr[i]=sc.nextInt();
        }
        int minNum = arr[0];

        for(int i=0;i<arr.length;i++){
            if (arr[i] < minNum){
                minNum = arr[i];
            }
        }

        System.out.println("Minimum nuber in the array is : "+minNum);

        sc.close();
    }
}