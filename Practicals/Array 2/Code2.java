/*
2.WAP to print the sum of elements divisible by 3 in the array, where you have to take the size and elements from the user.

Example:
Enter size: 8
Array:
9 13 5 13 6 22 36 10

Output :
Elements divisible by 3 : 9 6 36
Sum of elements divisible by 3 is: 51

*/
import java.util.*;
class Code2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        int sum = 0;
        System.out.print("Elements divisible by 3 : ");
        
        for(int i=0;i<arr.length;i++){
            if (arr[i] % 3 == 0) {
                System.out.print(arr[i]+" ");
                sum+=arr[i];
            }    
        }
        System.out.println();
        
        System.out.print("Sum of Elements divisible by 3 : "+sum);

        sc.close();
    }    
}
