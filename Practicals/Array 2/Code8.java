/*
8.WAP to print the elements in an array which are greater than 5 but less than 9, where you have to take the size and elements from the user.

Example:

Input:

Enter the array size: 5
Enter Elements 1: 4
Enter Elements 2: 8
Enter Elements 3: 6
Enter Elements 4: 9
Enter Elements 5: 41

Output:
8 is greater than 5 but less than 9    
6 is greater than 5 but less than 9

*/

import java.util.*;

class Code8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        
        int arr[] = new int[size];
        for(int i=0;i<arr.length;i++){
            System.out.print("Enter Elements "+(i+1)+": ");
            arr[i]=sc.nextInt();
        }
        
        for(int i=0;i<arr.length;i++){
            if (5 < arr[i] && arr[i] < 9) {
                System.out.println(arr[i]+" is greater than 5 but less than 9");
            }
        }
        sc.close();
    }
}