/*
4.WAP to search a specific element in an array and return its index. Ask the user to provide the number to search, also take size and elements input from the user.

Example:

Enter size: 5
Enter elements: 12 144 13 156 8

Enter the number to search in array: 8

Output:

vowel a found at index O
vowel E found at index 2
vowel O found at index 4

*/

import java.util.*;
class Code4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        System.out.println("Enter elements");

        for(int i=0;i<size;i++){
            arr[i] = sc.nextInt();
        }
        
        System.out.print("Enter the number to search in array : ");

        int num = sc.nextInt();
        boolean search = false;
        int index = -1;

        for(int i=0;i<arr.length;i++){
            if (arr[i]==num){
                search = true;
                index = i;
                break;
            }
        }

        if (search) {
            System.out.println(num+" found at index "+index);
        }
        else{
            System.out.println(num +" is not in the array.");
        }
        sc.close();
    }    
}
