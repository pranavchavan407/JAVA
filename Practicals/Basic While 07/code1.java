/*
 * Write a program to print the character for ASCII value in range 90 - 65
 */

class code1 {
    public static void main(String[] args) {
        int x =90;
        while (x>=65) {
            System.out.println((char)x);
            x--;
        }
    }
}
