/*
 * Write a program to print the numbers divisible by 4 and 7 in the range 50 - 100.
 *  */

 class code3 {
    public static void main(String[] args) {
        int x =50;
        while (x<=100) {
            if (x%4==0 && x%7==0) {
                System.out.println(x);
            }
            x++;
        }
    }
}
