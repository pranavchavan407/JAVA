import java.util.*;
class SpaceDemo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the rows : ");
		int row=sc.nextInt();

		for(int i=1; i<=row; i++){
			int ch=64+row;
		
			for(int sp=1; sp<i; sp++){
				System.out.print("  ");
			}
			for(int j=1; j<=row-i+1; j++){
				System.out.print((char)ch+" ");
				ch--;
			}
			System.out.println();
		}
	}
}
