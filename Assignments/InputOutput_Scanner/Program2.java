import java.util.*;
class InputDemo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter num ");
		int num=sc.nextInt();

		if(num>=18){
			System.out.println("Voter is eligible for voting");
		}else if(num<0||num>18){
			System.out.println("Voter is not eligible for voting");
		}else{
			System.out.println("Invalid num");
		}
	}
}
