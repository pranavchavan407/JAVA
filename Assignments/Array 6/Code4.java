import java.util.*;
class Code4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();

        int arr1[] = new int[size];
        int arr2[] = new int[size];
        
        System.out.println("Enter elements for array 1");

        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr1[i] = sc.nextInt();
        }
        
        System.out.println("Enter elements for array 2");
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr2[i] = sc.nextInt();
        }

        System.out.print("Common elements in the given arrays are: ");

        for(int i=0;i<arr1.length;i++){
            for(int j=0;j<arr2.length;j++){
                if (arr1[i]==arr2[j]) {
                    System.out.print(arr1[i] + " ");
                    break;
                }
            }
        }
        sc.close();
    }    
}
