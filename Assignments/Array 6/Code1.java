import java.util.*;
class Code1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        boolean descending = true;

        for(int i=1;i<arr.length;i++){
            if (arr[i]>arr[i-1]) {
                descending = false;
                break;
            }    
        }
        
        if (descending) {
            System.out.println("The array is in descending order.");
        }
        else{
            System.out.println("The array is NOT in descending order.");
        }

        sc.close();
    }    
}
