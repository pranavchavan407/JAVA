import java.util.*;
class Code5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array1 size: ");
        int size1 = sc.nextInt();
        System.out.print("Enter the array2 size: ");
        int size2 = sc.nextInt();

        int arr1[] = new int[size1];
        int arr2[] = new int[size2];
        
        System.out.println("Enter elements for array 1");

        for(int i=0;i<size1;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr1[i] = sc.nextInt();
        }
        
        System.out.println("Enter elements for array 2");
        
        for(int i=0;i<size2;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr2[i] = sc.nextInt();
        }

        int mergeArray[]=new int[size1+size2];

        int index = 0;

        for(int i=0;i<arr1.length;i++){
            mergeArray[index] = arr1[i];
            index++;
        }

        for(int j=0;j<arr2.length;j++){
            mergeArray[index] = arr2[j];
            index++;
        }

        System.out.println("Merged array is ");
        for(int i=0;i<mergeArray.length;i++){
            System.out.print(mergeArray[i]+" ");
        }
        sc.close();    
    }
}
