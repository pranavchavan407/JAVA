/*
10.Write a program to convert all negative numbers into their squares in a given array.

input :
-2 5 -6 7 -3 8 

output :
4 5 36 7 9 8
*/

class Code10 {
    public static void main(String[] args) {
        int arr[] = new int[]{-2,5,-6,7,-3,8};

        int newArr[] = new int[arr.length];

        for(int i=0;i<arr.length;i++){
            if(arr[i]<0){
                newArr[i] = arr[i]*arr[i];
            }
            else{
                newArr[i] = arr[i];
            }
        }
        
        for(int i=0;i<arr.length;i++){
            System.out.print(newArr[i]+" ");
        }
    }    
}
