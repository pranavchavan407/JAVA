/*
1. Write a program to add 15 in all elements of the array and print it.
*/

class Code1 {
    public static void main(String[] args) {
        int arr[] = new int[]{10,20,30,40,50,60};

        int newArr[] = new int[arr.length];

        for(int i=0;i<arr.length;i++){
            newArr[i] = arr[i]+15;    
        }
        
        System.out.print("New Array Elements are : ");
        
        for(int i=0;i<arr.length;i++){
            System.out.print(newArr[i]+",");
        }
    }    
}
