/*
Write a program to print the sum of digits in the given number.

Input: 9307922405
Output: sum of digits in 9307922405 is 41

*/
import java.util.*;
class Code10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Number: ");
        long num = sc.nextLong();
        sc.close();
        int sum = 0;
        while (num>0) {
            long digit = num%10;
            sum+=digit;
            num/=10;
        }
        System.out.println("sum of digits in "+num+" is "+ sum);
    }
}
