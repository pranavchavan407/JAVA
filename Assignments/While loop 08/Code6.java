/*
Write a program to print each digit on a new line of a given number
using a while loop

Input: num = 9307
Output: 7
        0
        3
        9

*/
import java.util.*;
class Code6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Number: ");
        int num = sc.nextInt();
        sc.close();
        while (num>0) {
            int digit = num%10;
            System.out.println(digit);
            num/=10;
        }
    }

}
