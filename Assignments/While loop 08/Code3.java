/*
 * Write a program to print the character sequence given below when the range given is
 
 input: start = 1, end = 6
 
 Output: A B C D E F

 */

 class Code3 {
    public static void main(String[] args) {
        int num=1;
        char ch = 'A';
        while (num<=6) {
            System.out.print(ch+" ");
            num++;
            ch++;
        }
    }
}
