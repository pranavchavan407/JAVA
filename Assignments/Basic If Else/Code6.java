class Code6 {
    public static void main(String[] args) {
        int num1 = 28;

        if (num1%3==0 || num1%7==0){
            if (num1%7==0 && num1%3==0){
                System.out.println(num1+" Divisible by 3 & 7");
            }
            else if (num1%3==0){
                System.out.println(num1+" Divisible by 3");
            }
            else{
                System.out.println(num1+" Divisible by 7");
            }
        }
        else {
            System.out.println(num1+" is not divisible by 3 or 7");
        }
    }
}
