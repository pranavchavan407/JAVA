/*
3. WAP to check if an array is a palindrome or not .

Input:
Enter the size of the array:6

Enter the elements of the array:
1
2
3
3
2
1

Output:
The given array is a palindrome array.
*/
import java.util.*;
class Code3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        boolean palindrome = true;

        for(int i=0;i<size/2;i++){
            if(arr[i]!=arr[size-i-1]){
                palindrome = false;
                break;
            }
        }
        
        if (palindrome) {
            System.out.println("The array is palindrome.");
        } 
        else {
            System.out.println("The array is NOT a palindrome.");
        }
        
        sc.close();
    }    
}
