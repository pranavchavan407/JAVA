/*
2. WAP to print the sum of odd and even numbers in an array.

Enter the size of the array:6

Enter the elements of the array:
10
15
9
1
12
15

Output:
Odd Sum = 40
Even Sum = 22
*/
import java.util.*;
class Code2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        int oddsum = 0;
        int evensum = 0;

        for(int i=0;i<size;i++){
            if(arr[i]%2==0){
                evensum+=arr[i];
            }
            else{
                oddsum+=arr[i];
            }
        }
        System.out.println("Odd Sum = "+oddsum);
        System.out.println("Even Sum = "+evensum);
        
        sc.close();
    }    
}
