/*
Enter the number of rows: 3

1 2 3
2 3
3

Enter the number of rows: 4

1 2 3 4
2 3 4
3 4
4
 
*/
import java.util.*;
class Code8{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");
        int rows = sc.nextInt();
        sc.close();

        for (int i = 1; i <= rows; i++) {
            for (int j = i; j <= rows; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }
}