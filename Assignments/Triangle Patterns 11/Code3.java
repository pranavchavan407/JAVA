/*
Enter the number of rows: 3

A
B C
C D E

Enter the number of rows: 4

A
B C
C D E
D E F G
 
*/
import java.util.*;
class Code3{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");
        int rows = sc.nextInt();
        sc.close();

        char ch1 = 'A';

        for (int i = 1; i <=rows; i++) {
            char ch2 = ch1;
            for (int j = 1; j<= i; j++) {
                System.out.print(ch2+" ");
                ch2++;
            }
            ch1++;
            System.out.println();
        }
    }
}